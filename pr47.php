<?php
namespace Html;
class Table{
    public $title = "";
    public $numrow = 0;
    public function message(){
        echo "<p>Table has '{$this->title}'has '{$this->numrow}' row.</p>";
    }
}
class row{
    public $numcells=0;
    public function message()
    {
        echo "<p>The row has '{$this->numcells}' cells.";
    }
}
use Html\table as H;
$table = new table();
$table->title="My table";
$table->numrow=5;
$row = new row();
$row->numcells = 3;
?>
<html>
    <body>
        <?php
        $table->message();
        $row->message();
        ?>
        </body>
</html>