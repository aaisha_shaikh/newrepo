<?php
abstract class parentclass
{
    abstract protected function prefixname($name);
}
class childclass extends parentclass{
    public function prefixname($name,$sepretor=".",$greet="Dear")
    {
        if($name =="John Doe")
        {
            $prefix = "Mr.";
        } elseif ($name == "Jane Doe")
        {
            $prefix = "Mrs.";
        }else{
            $prefix = "";
        }
        return "{$greet} {$prefix} {$name}";
        }
    }
$class = new childclass;
echo $class->prefixname("John Doe");
echo "<br>";
echo $class->prefixname("Jane Doe");
?>