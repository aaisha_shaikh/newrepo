<?php
abstract class car{
    public $name;
    public function __construct($name)
    {
        $this->name=$name;
    }
    abstract public function intro() : string;
}
class Audi extends car{
    public function intro(): string{
        return "I am an $this->name";
    }
}
class Volovo extends car{
    public function intro() : string{
        return "I am a $this->name";
    } 
}
$audi= new audi("Audi");
echo $audi->intro();
echo "<br>";
$volovo = new volovo("Volvo");
echo $volovo->intro();
echo "<br>";
?>