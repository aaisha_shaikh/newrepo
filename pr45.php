<?php
class greeting
{
    protected static function welcome()
    {
        echo "Hello World!";
    }
}
class SomeOtherClass extends greeting
{
    public $websitename;
    public function __construct(){
        
        $this->websitename=parent::welcome();
    }
}
$obj=new SomeOtherClass;
echo $obj->websitename;
?>