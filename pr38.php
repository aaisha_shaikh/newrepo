<?php
class fruit
{
    public $name;
    public $color;
    public function __construct($name,$color)
    {
        $this->name=$name;
        $this->color=$color;
    }
    protected function intro()
    {
        echo "The fruit is {$this->name} and the color is {$this->color}.";
    }
}
class strawberry extends fruit
{
    public function message()
    {
        echo "Am I fruit or berry?";
        $this->intro();
    }
}
$strawberry = new strawberry("starwberry","red");
$strawberry->message();
//$strawberry->intro();
?>